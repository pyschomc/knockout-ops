# Knockout Ops

Miscellaneous files related to deployment and operations

* logrotate files belong within /etc/logrotate.d
* nginx files belong within /etc/nginx/sites-available and they should be symlinked individually to /etc/nginx/sites-enabled
* systemd unit files belong within /etc/systemd/system
* sudoers are to be appended to /etc/sudoers or placed within /etc/sudoers.d
